package com.empowerment.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource; 

public class DatabbaseHelper{  
	public static DatabbaseHelper dataServiceHelper = null;
	
	private Connection con = null;
	
	DataSource dataSource = null;
	
	InitialContext initialContext = null;
	
	

	public Connection getConnection() throws SQLException {

		try{
			//System.out.println("DB Connection Started");
			Properties prop=new Properties();
			InputStream in = getClass().getResourceAsStream("db.properties");
			prop.load(in);
            in.close();
			
            //System.out.println("Property File Loaded");
			String driver = prop.getProperty("jdbc.driver");
			if (driver != null) {
			   Class.forName(driver) ;
			}
			String url = prop.getProperty("jdbc.url");
			String userName =prop.getProperty("jdbc.user");
			String password =prop.getProperty("jdbc.password");
				         
		    con=DriverManager.getConnection(url,userName,password); 
		    //System.out.println("DB Connection Completed");
		
		}
		catch(Exception e)
		{
			System.out.println("DatabaseHelper - Exception "+ e);
		}
		 finally {
			 
	        }
		return con;
		 
	} 	

	 
	public void closeConnection() throws SQLException {
		
		if (isConnectionOpen()) {
		
		con.close();
		
		con = null;
		
		}
	
		}

	
	public boolean isConnectionOpen() {
		
		return (con != null);
		
		}
		
	public static DatabbaseHelper getInstance() {
		
		if (dataServiceHelper == null) {
		
		dataServiceHelper = new DatabbaseHelper();
	
		}
		
		return dataServiceHelper;
		
		}

}  