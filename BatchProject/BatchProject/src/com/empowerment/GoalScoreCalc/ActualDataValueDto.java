package com.empowerment.GoalScoreCalc;

import java.sql.Date;
import java.util.ArrayList;

public class ActualDataValueDto {
	private int c_pk_goal_id;
	private int c_pk_emp_id;
	private String c_pk_goal_frequency_of_measurement;
	private String  c_pk_goal_calculation_method;
	private String c_dp_start_date;
    private String c_dp_end_date;
    
    
	
	
	public String getC_dp_start_date() {
		return c_dp_start_date;
	}
	public void setC_dp_start_date(String c_dp_start_date) {
		this.c_dp_start_date = c_dp_start_date;
	}
	public String getC_dp_end_date() {
		return c_dp_end_date;
	}
	public void setC_dp_end_date(String c_dp_end_date) {
		this.c_dp_end_date = c_dp_end_date;
	}
	public int getC_pk_goal_id() {
		return c_pk_goal_id;
	}
	public void setC_pk_goal_id(int c_pk_goal_id) {
		this.c_pk_goal_id = c_pk_goal_id;
	}
	public int getC_pk_emp_id() {
		return c_pk_emp_id;
	}
	public void setC_pk_emp_id(int c_pk_emp_id) {
		this.c_pk_emp_id = c_pk_emp_id;
	}
	public String getC_pk_goal_frequency_of_measurement() {
		return c_pk_goal_frequency_of_measurement;
	}
	public void setC_pk_goal_frequency_of_measurement(String c_pk_goal_frequency_of_measurement) {
		this.c_pk_goal_frequency_of_measurement = c_pk_goal_frequency_of_measurement;
	}
	public String getC_pk_goal_calculation_method() {
		return c_pk_goal_calculation_method;
	}
	public void setC_pk_goal_calculation_method(String c_pk_goal_calculation_method) {
		this.c_pk_goal_calculation_method = c_pk_goal_calculation_method;
	}
	
	
}
