package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.empowerment.util.DatabbaseHelper;


public class RuleEngineFetch {
 


 public String getAchievedValue(String dataPointAlias, int empId, int teamId,int processID, java.sql.Date startDate, java.sql.Date endDate,String frequency) throws SQLException
 {
       String SQLSTATEMENT =null;
	   Connection con = null;
	   ResultSet rs = null;
	   String valueDataPoint =null;
	   PreparedStatement preparedStatement =null;
	   con = DatabbaseHelper.getInstance().getConnection();
	   RuleEngineDao ruleenginedao =new RuleEngineDao();
      //Check for the tokens present in t_dp_rule_evaluation_data database or not.
	   SQLSTATEMENT="Select evaluation.c_dp_derived_data_value from t_dp_rule_evaluation_data evaluation, t_dp_epmo_process_rule_mapping mapping"
	     		+ " where mapping.c_dp_rule_id =evaluation.c_dp_rule_id and "
	     		+ "evaluation.c_dp_process_id= mapping.c_dp_epmo_process_id and "
	     		+ "c_dp_emp_id=? and  c_dp_start_date =? and c_dp_end_date=? and "
	     		+ "mapping.c_dp_calculation_frequency=? and "
	     		+ "(mapping.c_dp_derived_datapoint_alias =?  or evaluation.c_dp_derived_data_element_name=?)";
      preparedStatement = con.prepareStatement(SQLSTATEMENT);
      preparedStatement.setInt(1, empId);
      preparedStatement.setDate(2, startDate);
      preparedStatement.setDate(3, endDate);
      preparedStatement.setString(4, frequency);
      preparedStatement.setString(5, dataPointAlias);
      preparedStatement.setString(6,dataPointAlias);
      
      rs=preparedStatement.executeQuery();
     while(rs.next()){
	   valueDataPoint =rs.getString("c_dp_derived_data_value");
     }
     
      if(valueDataPoint==null)   {
	   ruleenginedao.getRules( dataPointAlias,empId,  teamId, processID, startDate, endDate,frequency);
	   rs=preparedStatement.executeQuery();
	   while(rs.next())   {
		   valueDataPoint =rs.getString("c_dp_derived_data_value");
	   }
     }
	  
   
    
   return valueDataPoint;
 
}
}
