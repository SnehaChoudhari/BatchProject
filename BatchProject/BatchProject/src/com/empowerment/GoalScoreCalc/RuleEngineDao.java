package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.naming.NamingException;


import com.empowerment.util.DatabbaseHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Date;

public class RuleEngineDao {
	//to get List of Rule Ids from T_PeakePMODetails peak, T_DP_TEAM_DERIVED_DATAPOINT_MAPPING mapping, T_DP_Team_New team, T_DP_TEAM_MEMBERS members table through joins
	//Joins Defined Below
	//peak.ProcessId = mapping.C_PK_EPMO_PROCESS_ID and 
	//team.C_Pk_team_id = mapping.C_Pk_team_id and
	//team.C_PK_TEAM_ID =member.C_PK_TEAM_ID
 
 
 public void getRules(java.sql.Date startDate, java.sql.Date endDate, String frequency) throws SQLException{
	 
	 Connection con = null;
	 ArrayList<RuleEngineDTO> ruleEngineDToArray = new ArrayList<RuleEngineDTO>();
	 String SQLSTATEMENT = null;
	 
	  SQLSTATEMENT= "select team.C_Pk_team_id ,team.c_pk_epmo_process_id ,mapping.c_dp_derived_datapoint_alias,mapping.C_DP_RULE_ID, member.C_PK_EMP_ID "
                   + "from t_dp_epmo_process_rule_mapping mapping, T_DP_Team team, T_DP_TEAM_MEMBERS member, T_DP_Rules rule "
	 		+ "where "
	 		+ "team.c_pk_epmo_process_id= mapping.c_dp_epmo_process_id and "
	 		+ "mapping.C_DP_RULE_ID =rule.C_DP_RULE_ID and "
	 		+ "team.C_PK_TEAM_ID =member.C_PK_TEAM_ID and "
	 		+ "team.c_pk_status='active' and "
	 		+ "member.c_pk_allocation_start_date<=? and "
	 		+" ( member.c_pk_allocation_end_date>=?  OR member.c_pk_allocation_end_date IS NULL) and "
	 		+ "c_dp_calculation_frequency = ? " 
	 		+ "ORDER BY mapping.c_dp_derived_datapoint_alias";
		 
	 RuleEngineDTO ruleEngineDto = null;
	
	 try {
	 con = DatabbaseHelper.getInstance().getConnection();
	 PreparedStatement preparedStatement = con.prepareStatement(SQLSTATEMENT);
	 preparedStatement.setDate(1, startDate);
	 preparedStatement.setDate(2, endDate);
	 preparedStatement.setString(3, frequency);
	 ResultSet rs = preparedStatement.executeQuery();
	 con.setAutoCommit(false);
	 while ( rs.next() ){
		 ruleEngineDto = new RuleEngineDTO();
		 ruleEngineDto.setC_Pk_team_id(rs.getInt("C_Pk_team_id"));
		 ruleEngineDto.setC_PK_EPMO_PROCESS_ID(rs.getInt("c_pk_epmo_process_id"));
		 ruleEngineDto.setC_PK_EMP_ID(rs.getInt("C_PK_EMP_ID"));
		 ruleEngineDto.setC_DP_RULE_ID(rs.getInt("C_DP_RULE_ID"));
		 ruleEngineDto.setStartDate(startDate);
		 ruleEngineDto.setEndDate(endDate);
		 ruleEngineDto.setFrequency(frequency);
		 //ruleEngineDto.setProcessedDate(rs.getDate("ProcessedDate"));
		 ruleEngineDto.setC_DP_Derived_Datapoint_Alias(rs.getString("c_dp_derived_datapoint_alias"));
		 ruleEngineDToArray.add(ruleEngineDto);
       }
	 ExcecuteRule_updated(ruleEngineDToArray,con);
    }
 
	 catch (SQLException se)
	  {
	  System.out.println(se.toString());
	  }
	 
 }
 //Loop through each Rule_ID get Rule Details from T_DP_RULES Table
 //Get Rule Parameter for the Rule Selected from T_DP_RULE_PARAMETERS table.
 
 public void getRules(String dataPointAlias, int empId, int teamId, int processID, java.sql.Date startDate, java.sql.Date endDate, String frequency) throws SQLException{
	 
	 Connection con = null;
	 ArrayList<RuleEngineDTO> ruleEngineDToArray = new ArrayList<RuleEngineDTO>();
	 try { 
	 
	 String SQLSTATEMENT= "select team.C_Pk_team_id ,team.c_pk_epmo_process_id ,mapping.c_dp_derived_datapoint_alias,mapping.C_DP_RULE_ID, member.C_PK_EMP_ID "
                   + "from t_dp_epmo_process_rule_mapping mapping, T_DP_Team team, T_DP_TEAM_MEMBERS member, T_DP_Rules rule "
	 		+ "where "
	 		+ "team.c_pk_epmo_process_id= mapping.c_dp_epmo_process_id and "
	 		+ "mapping.C_DP_RULE_ID =rule.C_DP_RULE_ID and "
	 		+ "team.C_PK_TEAM_ID =member.C_PK_TEAM_ID and "
	 		+ "team.c_pk_status='active' and "
	 		+ "member.c_pk_allocation_start_date<=? and "
	 		+" ( member.c_pk_allocation_end_date>=?  OR member.c_pk_allocation_end_date IS NULL) and "
	 	    + "member.c_pk_emp_id =? and "
	 		+ "team.C_Pk_team_id =? and "
	 		+ "mapping.c_dp_epmo_process_id=? and "
	 		+ "(mapping.c_dp_derived_datapoint_alias =?  or rule.c_dp_derived_datapoint= ?) and "
	 		+ "rule.c_dp_applicable_frequency= ? "
	 		+ "ORDER BY mapping.c_dp_derived_datapoint_alias";
	 
	 	
	 RuleEngineDTO ruleEngineDto = null;
	
	
	 con = DatabbaseHelper.getInstance().getConnection();
	 PreparedStatement preparedStatement = con.prepareStatement(SQLSTATEMENT);
	 preparedStatement.setDate(1, startDate);
	 preparedStatement.setDate(2, endDate);
	 preparedStatement.setInt(3, empId);
	 preparedStatement.setInt(4, teamId);
	 preparedStatement.setInt(5, processID);
	 preparedStatement.setString(6, dataPointAlias);
	 preparedStatement.setString(7, dataPointAlias);
     preparedStatement.setString(8, frequency);
	 ResultSet rs = preparedStatement.executeQuery();
	 con.setAutoCommit(false);
	 while ( rs.next() ){
		 ruleEngineDto = new RuleEngineDTO();
		 ruleEngineDto.setC_Pk_team_id(rs.getInt("C_Pk_team_id"));
		 ruleEngineDto.setC_PK_EPMO_PROCESS_ID(rs.getInt("c_pk_epmo_process_id"));
		 ruleEngineDto.setC_PK_EMP_ID(rs.getInt("C_PK_EMP_ID"));
		 ruleEngineDto.setC_DP_RULE_ID(rs.getInt("c_DP_RULE_ID"));
		 //ruleEngineDto.setProcessedDate(rs.getDate("ProcessedDate"));
		 ruleEngineDto.setStartDate(startDate);
		 ruleEngineDto.setEndDate(endDate);
		 ruleEngineDto.setC_DP_Derived_Datapoint_Alias(rs.getString("c_dp_derived_datapoint_alias"));
		 ruleEngineDToArray.add(ruleEngineDto);
       }
	 ExcecuteRule_updated(ruleEngineDToArray,con);
    
	 }
	 
	 
	 catch (SQLException se)
	  {
	  System.out.println(se.toString());
	  }
	 
 }

public void ExcecuteRule_updated( ArrayList<RuleEngineDTO> ruleEngineDToArray, Connection con) throws SQLException{
	 
	 RuleEngineDTO ruleEngineDto;
	 TDpRuleParametersDTO rulesParameter;
	 PreparedStatement ps =null;
	 PreparedStatement preparedStatementRuleQuery =null;
	 PreparedStatement preparedStatement=null ;
	 PreparedStatement preparedStatementRule=null;
	 ArrayList<TDpRuleParametersDTO> rulesParameterArray = new ArrayList<TDpRuleParametersDTO>();
	 Date StartDate =null;
	 Date EndDate=null;
	 try {
		 String insertInRuleEvaluationData = "insert into t_dp_rule_evaluation_data values(?,?,?,?,?,?,?,?)";
		 ps = con.prepareStatement(insertInRuleEvaluationData);
	 for(int i=0;i<ruleEngineDToArray.size();i++)
	 {
		 ruleEngineDto = ruleEngineDToArray.get(i);
		 int ruleID = ruleEngineDto.getC_DP_RULE_ID();
		 Map<String,Object> map=new HashMap<String,Object>();
		 map.put("EmpId", ruleEngineDto.getC_PK_EMP_ID());
		 map.put("ProcessedDate", ruleEngineDto.getStartDate());
		 map.put("ProcessId", ruleEngineDto.getC_PK_EPMO_PROCESS_ID());
		 StartDate =ruleEngineDto.getStartDate();
		 EndDate =ruleEngineDto.getEndDate();
			 
		 String SQLSTATEMENT= "select * from T_DP_RULES where C_DP_RULE_ID = ?";
		 String SQLSTATEMENTRuleDetails= "select * from T_DP_RULE_PARAMETERS where C_DP_RULE_ID = ?";
		 TDpRulesDTO ruleDto = null;
	     preparedStatement = con.prepareStatement(SQLSTATEMENT);
		 preparedStatement.setInt(1, ruleID);
		 ResultSet rs = preparedStatement.executeQuery();
		 TDpRulesDTO rules =new TDpRulesDTO();
		 //Rule_ID get Rule Details from T_DP_RULES Table
		 while ( rs.next() )
		    {
			 rules = new TDpRulesDTO();
			 rules.setRule_ID(rs.getInt("C_DP_RULE_ID"));
			 rules.setRule_Query(rs.getString("C_DP_RULE_QUERY"));
			 rules.setApplicability(rs.getString("C_DP_APPLICABLE_FREQUENCY"));
			 rules.setApplicable_Freq(rs.getString("C_DP_APPLICABLE_FREQUENCY"));
			 rules.setDerived_Data_Point(rs.getString("C_DP_DERIVED_DATAPOINT"));
			 rules.setDerived_Datapoint_Algorithm(rs.getString("C_DP_DERIVED_DATAPOINT_ALGORITHM"));
			 rules.setDerived_Datapoint_Description(rs.getString("C_DP_DERIVED_DATAPOINT_DESCRIPTION"));
			 
		    }
		

		  preparedStatementRule = con.prepareStatement(SQLSTATEMENTRuleDetails);
		  preparedStatementRule.setInt(1, ruleID);
		  ResultSet rsRuleDetails = preparedStatementRule.executeQuery();
		    
			//Get Rule Paramater for the Rule Selected from T_DP_RULE_PARAMETERS table
			 Map<Integer,Object> mapParameter=new HashMap<Integer,Object>();
			 while ( rsRuleDetails.next() )
			    {
				 rulesParameter = new TDpRuleParametersDTO();
				 rulesParameter.setRule_ID(rsRuleDetails.getInt("C_DP_RULE_ID"));
				 rulesParameter.setParameter_Name(rsRuleDetails.getString("C_DP_PARAMETER_NAME"));
				 rulesParameter.setParameter_Sequence(rsRuleDetails.getString("C_DP_PARAMETER_SEQUENCE"));
				 rulesParameter.setParameter_Query(rsRuleDetails.getString("C_DP_PARAMETER_QUERY"));
				 rulesParameter.setParameter_Type(rsRuleDetails.getString("C_DP_PARAMETER_TYPE"));
				 for (Map.Entry<String,Object> entry : map.entrySet())
				 {
				     System.out.println(entry.getKey() + "/" + entry.getValue());
				     System.out.println("Parameter Name" + rsRuleDetails.getString("C_DP_PARAMETER_NAME"));
				     if(entry.getKey().equals(rulesParameter.getParameter_Name()))
				     {
				    	 if(rulesParameter.getParameter_Type().equals("int"))
				    	 mapParameter.put(new Integer(rulesParameter.getParameter_Sequence()), entry.getValue());
				    	 if(rulesParameter.getParameter_Type().equals("datetime"))
				    	 mapParameter.put(new Integer(rulesParameter.getParameter_Sequence()), entry.getValue());
				  	
				     }
				 }
			  rulesParameterArray.add(rulesParameter);
			  }
			 String ruleQuery = rules.getRule_Query();
			 preparedStatementRuleQuery = con.prepareStatement(ruleQuery);
			 System.out.println("ruleQuery" + ruleQuery);
			 for (Map.Entry<Integer,Object> entry : mapParameter.entrySet()) {
			 System.out.println(entry.getKey() + "/parameter" + entry.getValue());
			 preparedStatementRuleQuery.setObject(entry.getKey() ,entry.getValue());
			 }
			 ResultSet rsResult = preparedStatementRuleQuery.executeQuery();
			 
			 while ( rsResult.next()) {
				  String value = rsResult.getString("value");
				  float valueData=0;
				  if(value!=null)
					  valueData =Float.parseFloat(value) ; 
				  System.out.println("Rule Id:" +ruleEngineDto.getC_DP_RULE_ID()+ " processId :"+
				  ruleEngineDto.getC_PK_EPMO_PROCESS_ID()+" Team Id"+ruleEngineDto.getC_Pk_team_id()+" Emp Id :"+
				  ruleEngineDto.getC_PK_EMP_ID()+"Derived Dataelement Name :"+ruleEngineDto.getC_DP_Derived_Datapoint_Alias()+
				  "Derived Dataelement value :"+value +" Start date " + ruleEngineDto.getProcessedDate()+" End date " + ruleEngineDto.getProcessedDate());
				  
				  
				  ps.setInt(1,ruleEngineDto.getC_DP_RULE_ID());
				  ps.setInt(2,ruleEngineDto.getC_PK_EPMO_PROCESS_ID());
				  ps.setInt(3, ruleEngineDto.getC_Pk_team_id());
				  ps.setInt(4,ruleEngineDto.getC_PK_EMP_ID());
				  ps.setString(5,rules.getDerived_Data_Point());
				  ps.setFloat(6,valueData);
				  ps.setDate(7,StartDate);
				  ps.setDate(8,EndDate);
				  ps.addBatch();
				 
				  }
			
		         }
	 if(ps!=null)
	 {
      ps.executeBatch();
	  con.commit();
	  ps.close();
	 }  
	 }
 catch (SQLException se) {
 System.out.println(se.toString());
  }
 DatabbaseHelper.getInstance().closeConnection();
 }
 
}
