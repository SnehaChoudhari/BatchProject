package com.empowerment.GoalScoreCalc;

public class TDpRuleParametersDTO {
	private int Rule_ID;
	private String Parameter_Name;
	private String Parameter_Sequence;
	private String Parameter_Query;
	private String Parameter_Type;
	public int getRule_ID() {
		return Rule_ID;
	}
	public void setRule_ID(int rule_ID) {
		Rule_ID = rule_ID;
	}
	public String getParameter_Name() {
		return Parameter_Name;
	}
	public void setParameter_Name(String parameter_Name) {
		Parameter_Name = parameter_Name;
	}
	public String getParameter_Sequence() {
		return Parameter_Sequence;
	}
	public void setParameter_Sequence(String parameter_Sequence) {
		Parameter_Sequence = parameter_Sequence;
	}
	public String getParameter_Query() {
		return Parameter_Query;
	}
	public void setParameter_Query(String parameter_Query) {
		Parameter_Query = parameter_Query;
	}
	public String getParameter_Type() {
		return Parameter_Type;
	}
	public void setParameter_Type(String parameter_Type) {
		Parameter_Type = parameter_Type;
	}

}
