package com.empowerment.GoalScoreCalc;

import java.sql.Date;
import java.util.ArrayList;

public class CalScoreDto {
	
	 private int CalGoalID;
     private int CalEmpID;
     private int CalTeamID;
     private Date CalStartDate;
     private Date CalEndDate;
     private double  Cal_Goal_achieved_value;
     private String CalProcessedDate;
     private int Cal_No_of_working_days;
     private String caldataSource;
     private Date calScoreCalDate;
	 private double Cal_section_weitage;
     private String Cal_goal_frequency_of_measurement;
     private String Cal_measurement_direction;
     private boolean Cal_proration_allowed;
     private double CalgoalTarget;
  //   private double calAllocationPerc;
 	 private double CalgoalWeightage;
 	 private double CalsubSectionWeitage;
 	 private double CaltaskPerformanceScoreCalculation;
 	 private double CalgoalScoreCalculation;
 	 private double CalsubSectionScoreCalculation;
 	 private double CalsectionScoreCalculation;
 	 private Date calPerformanceDate;
 	 private double calteamScoreCalculation;
 	 private int Cal_No_Of_Working_Days_In_Period;  
 	 
 	private  ArrayList<Double> calAllocationPercList=new ArrayList<>();//
 	 
 	
 	
	public ArrayList<Double> getCalAllocationPercList() {
		return calAllocationPercList;
	}
	public void setCalAllocationPercList(ArrayList<Double> calAllocationPercList) {
		this.calAllocationPercList = calAllocationPercList;
	}
	public int getCalGoalID() {
		return CalGoalID;
	}
	public void setCalGoalID(int calGoalID) {
		CalGoalID = calGoalID;
	}
	public int getCalEmpID() {
		return CalEmpID;
	}
	public void setCalEmpID(int calEmpID) {
		CalEmpID = calEmpID;
	}
	public int getCalTeamID() {
		return CalTeamID;
	}
	public void setCalTeamID(int calTeamID) {
		CalTeamID = calTeamID;
	}

	public Date getCalStartDate() {
		return CalStartDate;
	}
	public void setCalStartDate(Date calStartDate) {
		CalStartDate = calStartDate;
	}
	public Date getCalEndDate() {
		return CalEndDate;
	}
	public void setCalEndDate(Date calEndDate) {
		CalEndDate = calEndDate;
	}
	public double getCal_Goal_achieved_value() {
		return Cal_Goal_achieved_value;
	}
	public void setCal_Goal_achieved_value(double cal_Goal_achieved_value) {
		Cal_Goal_achieved_value = cal_Goal_achieved_value;
	}
	public String getCalProcessedDate() {
		return CalProcessedDate;
	}
	public void setCalProcessedDate(String calProcessedDate) {
		CalProcessedDate = calProcessedDate;
	}
	public int getCal_No_of_working_days() {
		return Cal_No_of_working_days;
	}
	public void setCal_No_of_working_days(int cal_No_of_working_days) {
		Cal_No_of_working_days = cal_No_of_working_days;
	}
	public double getCal_section_weitage() {
		return Cal_section_weitage;
	}
	public void setCal_section_weitage(double cal_section_weitage) {
		Cal_section_weitage = cal_section_weitage;
	}
	public String getCal_goal_frequency_of_measurement() {
		return Cal_goal_frequency_of_measurement;
	}
	public void setCal_goal_frequency_of_measurement(String cal_goal_frequency_of_measurement) {
		Cal_goal_frequency_of_measurement = cal_goal_frequency_of_measurement;
	}
	public String getCal_measurement_direction() {
		return Cal_measurement_direction;
	}
	public void setCal_measurement_direction(String cal_measurement_direction) {
		Cal_measurement_direction = cal_measurement_direction;
	}
	public boolean isCal_proration_allowed() {
		return Cal_proration_allowed;
	}
	public void setCal_proration_allowed(boolean cal_proration_allowed) {
		Cal_proration_allowed = cal_proration_allowed;
	}
	public double getCalgoalTarget() {
		return CalgoalTarget;
	}
	public void setCalgoalTarget(double calgoalTarget) {
		CalgoalTarget = calgoalTarget;
	}
	public double getCalgoalWeightage() {
		return CalgoalWeightage;
	}
	public void setCalgoalWeightage(double calgoalWeightage) {
		CalgoalWeightage = calgoalWeightage;
	}
	public double getCalsubSectionWeitage() {
		return CalsubSectionWeitage;
	}
	public void setCalsubSectionWeitage(double calsubSectionWeitage) {
		CalsubSectionWeitage = calsubSectionWeitage;
	}
	public double getCaltaskPerformanceScoreCalculation() {
		return CaltaskPerformanceScoreCalculation;
	}
	public void setCaltaskPerformanceScoreCalculation(double caltaskPerformanceScoreCalculation) {
		CaltaskPerformanceScoreCalculation = caltaskPerformanceScoreCalculation;
	}
	public double getCalgoalScoreCalculation() {
		return CalgoalScoreCalculation;
	}
	public void setCalgoalScoreCalculation(double calgoalScoreCalculation) {
		CalgoalScoreCalculation = calgoalScoreCalculation;
	}
	public double getCalsubSectionScoreCalculation() {
		return CalsubSectionScoreCalculation;
	}
	public void setCalsubSectionScoreCalculation(double calsubSectionScoreCalculation) {
		CalsubSectionScoreCalculation = calsubSectionScoreCalculation;
	}
	public double getCalsectionScoreCalculation() {
		return CalsectionScoreCalculation;
	}
	public void setCalsectionScoreCalculation(double calsectionScoreCalculation) {
		CalsectionScoreCalculation = calsectionScoreCalculation;
	}
	 public String getCaldataSource() {
			return caldataSource;
		}
		public void setCaldataSource(String caldataSource) {
			this.caldataSource = caldataSource;
		}
		public Date getCalScoreCalDate() {
			return calScoreCalDate;
		}
		public void setCalScoreCalDate(Date goalScoreCalcScoreCalcDate) {
			this.calScoreCalDate = goalScoreCalcScoreCalcDate;
		}
		public Date getCalPerformanceDate() {
			return calPerformanceDate;
		}
		public void setCalPerformanceDate(Date goalScoreCalcPerformancedate) {
			this.calPerformanceDate = goalScoreCalcPerformancedate;
		}
		/*public double getCalAllocationPerc() {
			return calAllocationPerc;
		}
		public void setCalAllocationPerc(double calAllocationPerc) {
			this.calAllocationPerc = calAllocationPerc;
		}*/
		public double getCalteamScoreCalculation() {
			return calteamScoreCalculation;
		}
		public void setCalteamScoreCalculation(double calteamScoreCalculation) {
			this.calteamScoreCalculation = calteamScoreCalculation;
		}
		public int getCal_No_Of_Working_Days_In_Period() {
			return Cal_No_Of_Working_Days_In_Period;
		}
		public void setCal_No_Of_Working_Days_In_Period(int cal_No_Of_Working_Days_In_Period) {
			Cal_No_Of_Working_Days_In_Period = cal_No_Of_Working_Days_In_Period;
		}
		
	 
		
}
