package com.empowerment.GoalScoreCalc;

import java.sql.Date;

public class ExpectedGoalDto {
      private int GoalID;
      private int EmpID;
      private int TeamID;
      private Date StartDate;
      private Date EndDate;
      private String ScoreCaldate;
      private String dataSource;
      private double  Goal_achieved_value;
      private String ProcessedDate;
      private int No_of_working_days;
      private double section_weitage;
      private String goal_frequency_of_measurement;
      private String measurement_direction;
      private String calculationMethod;;
      private boolean proration_allowed;
      private double goalTarget;
  	  private double goalWeightage;
  	  private double subSectionWeitage;
  	  private double allocationPerc;
  	  private String peakRole;
  	  private String sectionName;
  	  private int countOfEmpId;
  	  private double sumOfPercentageAlloc;
  	  private String sectionType;
  	  private int allocatedPeriod;
  	  private int No_Of_Working_Days_In_Period;
  	  
  	  
  	  
  	public String getCalculationMethod() {
		return calculationMethod;
	}
	public void setCalculationMethod(String calculationMethod) {
		this.calculationMethod = calculationMethod;
	}
	public String getPeakRole() {
		return peakRole;
	}
	public void setPeakRole(String peakRole) {
		this.peakRole = peakRole;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getScoreCaldate() {
		return ScoreCaldate;
	}
	public void setScoreCaldate(String scoreCaldate) {
		ScoreCaldate = scoreCaldate;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
      
	public double getSubSectionWeitage() {
		return subSectionWeitage;
	}
	public void setSubSectionWeitage(double subSectionWeitage) {
		this.subSectionWeitage = subSectionWeitage;
	}
	public int getGoalID() {
		return GoalID;
	}
	public void setGoalID(int goalID) {
		GoalID = goalID;
	}
	public int getEmpID() {
		return EmpID;
	}
	public void setEmpID(int empID) {
		EmpID = empID;
	}
	public int getTeamID() {
		return TeamID;
	}
	public void setTeamID(int teamID) {
		TeamID = teamID;
	}
	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getEndDate() {
		return EndDate;
	}
	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
	public String getProcessedDate() {
		return ProcessedDate;
	}
	public void setProcessedDate(String processedDate) {
		ProcessedDate = processedDate;
	}
	public int getNo_of_working_days() {
		return No_of_working_days;
	}
	public void setNo_of_working_days(int no_of_working_days) {
		No_of_working_days = no_of_working_days;
	}
	public double getGoal_achieved_value() {
		return Goal_achieved_value;
	}
	public void setGoal_achieved_value(double goal_achieved_value) {
		Goal_achieved_value = goal_achieved_value;
	}
	public double getSection_weitage() {
		return section_weitage;
	}
	public void setSection_weitage(double section_weitage) {
		this.section_weitage = section_weitage;
	}
	public String getGoal_frequency_of_measurement() {
		return goal_frequency_of_measurement;
	}
	public void setGoal_frequency_of_measurement(String goal_frequency_of_measurement) {
		this.goal_frequency_of_measurement = goal_frequency_of_measurement;
	}
	public String getMeasurement_direction() {
		return measurement_direction;
	}
	public void setMeasurement_direction(String measurement_direction) {
		this.measurement_direction = measurement_direction;
	}
	public boolean isProration_allowed() {
		return proration_allowed;
	}
	public void setProration_allowed(boolean proration_allowed) {
		this.proration_allowed = proration_allowed;
	}
	public double getGoalWeightage() {
		return goalWeightage;
	}
	public void setGoalWeightage(double goalWeightage) {
		this.goalWeightage = goalWeightage;
	}
	public double getGoalTarget() {
		return goalTarget;
	}
	public void setGoalTarget(double goalTarget) {
		this.goalTarget = goalTarget;
	}
	public double getAllocationPerc() {
		return allocationPerc;
	}
	public void setAllocationPerc(double allocationPerc) {
		this.allocationPerc = allocationPerc;
	}
	public int getCountOfEmpId() {
		return countOfEmpId;
	}
	public void setCountOfEmpId(int countOfEmpId) {
		this.countOfEmpId = countOfEmpId;
	}
	public double getSumOfPercentageAlloc() {
		return sumOfPercentageAlloc;
	}
	public void setSumOfPercentageAlloc(double sumOfPercentageAlloc) {
		this.sumOfPercentageAlloc = sumOfPercentageAlloc;
	}
	public String getSectionType() {
		return sectionType;
	}
	public void setSectionType(String sectionType) {
		this.sectionType = sectionType;
	}
	public int getAllocatedPeriod() {
		return allocatedPeriod;
	}
	public void setAllocatedPeriod(int allocatedPeriod) {
		this.allocatedPeriod = allocatedPeriod;
	}
	public int getNo_Of_Working_Days_In_Period() {
		return No_Of_Working_Days_In_Period;
	}
	public void setNo_Of_Working_Days_In_Period(int no_Of_Working_Days_In_Period) {
		No_Of_Working_Days_In_Period = no_Of_Working_Days_In_Period;
	}
	
	

	
      
}
