package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.empowerment.util.DatabbaseHelper;


public class AchievedValueMethodDao {
	
	public int getAchievedValue(int goalId, String startDate, String EndDate) throws SQLException
	{

		 Date formattedStartDate=null;
		 Date formattedEndDate=null;
		 String SQLSTATEMENT =null;
		 String SQLSTATEMENT1 =null;
		 String SQLSTATEMENT2 = null;
		 String SQLSTATEMENT3 = null;
		 String SQLSTATEMENT4 = null;
		 String SQLSTATEMENT5 = null;
		 int employeeId=0;
		 int teamId=0;
		 int groupId=0;
		 int ruleId=0;
		 String frequency = null;
		 String formula = null;
		 StringBuffer tokenName =new StringBuffer();
		 int achievedvalue = 0;
		 int epmoProcessId = 0;
		 ArrayList<String> tokens = new ArrayList<>();
		 ArrayList<String> tokenNameCheck = new ArrayList<>();
		 String values = null;
		 ArrayList<String> tokenNameCheckForElsePart = new ArrayList<>();
		 ArrayList<Integer> valuesForElsePart = new ArrayList<>();
		 Connection con = null;
		 ResultSet rs = null;
		 PreparedStatement preparedStatement =null;
		 con = DatabbaseHelper.getInstance().getConnection();
		 RuleEngineFetch ruleEngineFetch = new RuleEngineFetch();
		 
		 try
		 {
			 formattedStartDate=Date.valueOf(startDate); 
			 formattedEndDate=Date.valueOf(EndDate);
			 con.setAutoCommit(false); 
			 //Get employee Id, team id, group Id, formula.
			 SQLSTATEMENT = "Select c_pk_emp_id, c_pk_team_id, c_pk_group_id, c_pk_goal_formula,c_pk_goal_frequency_of_measurement from t_dp_goal where c_pk_goal_id = ?";
			 PreparedStatement ps = con.prepareStatement(SQLSTATEMENT);
			 ps.setInt(1, goalId);
			 rs=ps.executeQuery();
			 while(rs.next())
			 {
				 employeeId = rs.getInt("c_pk_emp_id");
				 teamId = rs.getInt("c_pk_team_id");
				 groupId = rs.getInt("c_pk_group_id");
				 formula = rs.getString("c_pk_goal_formula");
				 frequency=rs.getString("c_pk_goal_frequency_of_measurement");
			 }
			 
			 //Get Epmo process id.
			 SQLSTATEMENT1 ="Select c_pk_epmo_process_id from t_dp_team where c_pk_team_id =?";
			 PreparedStatement ps1 = con.prepareStatement(SQLSTATEMENT1);
			 ps1.setInt(1, teamId);
			 rs=ps1.executeQuery();
			 while(rs.next())
			 {
				 epmoProcessId = rs.getInt("c_pk_epmo_process_id");
			 }
			 
			 //Extracting all the tokens from the formula.
			 StringTokenizer st = new StringTokenizer(formula,"/+-*1234567890.()[]{}");
			 while(st.hasMoreTokens())
			 {
			 	tokens.add(st.nextToken());
			 }
			 
			 System.out.println("Token - "+tokens);
			
			 for(int i=0;i<tokens.size();i++)
			{
			  
			  //Condition to call rule execution function.
			  values =	ruleEngineFetch.getAchievedValue(tokens.get(i), employeeId, teamId, epmoProcessId, formattedStartDate, formattedEndDate, frequency);
			  if(values==null){
				  achievedvalue=0;
				  break;
			  }
			  else
			  {
				  formula=formula.replaceFirst(tokens.get(i), (values));
				  System.out.println(formula);
			
				//Replacing all Tokens with their Value.
				
				//Query to find Achieved value.
				SQLSTATEMENT4 ="Select "+formula+" ";
				PreparedStatement ps4 = con.prepareStatement(SQLSTATEMENT4);
				rs = ps4.executeQuery();
				while(rs.next())
				{
					achievedvalue = rs.getInt(1);
				}
				con.commit();
			  }
			}
		}
		 
		 catch(SQLException e)
		 {
			System.out.println(e.toString());
			con.rollback();
		 }
		 finally
		 {
			 if (preparedStatement != null) 
			 {
				preparedStatement.close();
			 }
			 if (con != null)
			 {
			 	DatabbaseHelper.getInstance().closeConnection();
			 }
			 rs.close(); 
		 }
		 return achievedvalue;
	}
	
}
