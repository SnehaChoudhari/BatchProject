package com.empowerment.GoalScoreCalc;

import java.sql.Date;

public class RuleEngineDTO {
	private int C_Pk_team_id ;
	private int C_PK_EPMO_PROCESS_ID;
	private int C_DP_RULE_ID;
	private int C_PK_EMP_ID;
	private Date ProcessedDate;
	private Date startDate;
	private Date endDate;
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	private String frequency;
	private String C_DP_Derived_Datapoint_Alias;
	public String getC_DP_Derived_Datapoint_Alias() {
		return C_DP_Derived_Datapoint_Alias;
	}
	public void setC_DP_Derived_Datapoint_Alias(String c_DP_Derived_Datapoint_Alias) {
		C_DP_Derived_Datapoint_Alias = c_DP_Derived_Datapoint_Alias;
	}
	public int getC_Pk_team_id() {
		return C_Pk_team_id;
	}
	public void setC_Pk_team_id(int c_Pk_team_id) {
		C_Pk_team_id = c_Pk_team_id;
	}
	public Date getProcessedDate() {
		return ProcessedDate;
	}
	public void setProcessedDate(Date processedDate) {
		ProcessedDate = processedDate;
	}
	public int getC_PK_EPMO_PROCESS_ID() {
		return C_PK_EPMO_PROCESS_ID;
	}
	public void setC_PK_EPMO_PROCESS_ID(int c_PK_EPMO_PROCESS_ID) {
		C_PK_EPMO_PROCESS_ID = c_PK_EPMO_PROCESS_ID;
	}
	public int getC_DP_RULE_ID() {
		return C_DP_RULE_ID;
	}
	public void setC_DP_RULE_ID(int c_DP_RULE_ID) {
		C_DP_RULE_ID = c_DP_RULE_ID;
	}
	public int getC_PK_EMP_ID() {
		return C_PK_EMP_ID;
	}
	public void setC_PK_EMP_ID(int c_PK_EMP_ID) {
		C_PK_EMP_ID = c_PK_EMP_ID;
	}
	
	
	 

}
