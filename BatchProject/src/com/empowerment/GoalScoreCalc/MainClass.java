package com.empowerment.GoalScoreCalc;

/**@author - Jayshree, Manish**/

import java.sql.Date;
//import org.apache.log4j.Logger;

public class MainClass {
//	final static Logger logger = Logger.getLogger(MainClass.class);

	/**
	 * @param Args
	 * @throws ClassNotFoundException
	 */

	public static void main(String Args[]) throws ClassNotFoundException {

		DailyGoalScoreCalcService dailyGoalScoreCalcService = new DailyGoalScoreCalcService();

		try {
			System.out.println("Daily Date - " + Args[0]);
			System.out.println("Month - " + Args[1]);
			System.out.println("Year - " + Args[2]);
			System.out.println("week start date - " + Args[3]);
			System.out.println("week end date - " + Args[4]);
			
//			String dailyDate = "2017-01-02";
//			Date inputdate = Date.valueOf(dailyDate);
//			System.out.println("dailyDate - " + dailyDate);
			String dailyDate=  Args[0];
			Date inputdate = Date.valueOf(dailyDate);
			dailyGoalScoreCalcService.dailyRecords(inputdate);

			// Monthly
//			int month = 1;
//			int year = 2017;
			int month = Integer.parseInt(Args[1]);
			int year = Integer.parseInt(Args[2]);
			dailyGoalScoreCalcService.monthlyRecords(month, year);

			// Weekly
//			String weekStart = "2017-01-01";
//			String weekEnd = "2017-01-07";
			String weekStart = Args[3];
			String weekEnd = Args[4];
			Date weekStartDate = Date.valueOf(weekStart);
			Date weekEndDate = Date.valueOf(weekEnd);
			dailyGoalScoreCalcService.weeklyRecords(weekStartDate, weekEndDate);

		} catch (Exception ex) {
			System.out.println("mainClass - Exception " + ex);

		}

	}
}          


