package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GoalScoreCalcDao {

	ResultSet rs=null;
	public ResultSet getMethod(String sqlStatement,Connection con)
	{
		try {
			PreparedStatement ps = con.prepareStatement(sqlStatement);
			rs =ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return rs;
	}
	
}
