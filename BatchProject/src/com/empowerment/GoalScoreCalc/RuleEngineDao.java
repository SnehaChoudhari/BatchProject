package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.naming.NamingException;

import com.empowerment.GoalScoreCalc.RuleEngineDTO;
import com.empowerment.GoalScoreCalc.TDpRuleParametersDTO;
import com.empowerment.GoalScoreCalc.TDpRulesDTO;
import com.empowerment.util.DatabbaseHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Date;

public class RuleEngineDao {
	//to get List of Rule Ids from T_PeakePMODetails peak, T_DP_TEAM_DERIVED_DATAPOINT_MAPPING mapping, T_DP_Team_New team, T_DP_TEAM_MEMBERS members table through joins
	//Joins Defined Below
	//peak.ProcessId = mapping.C_PK_EPMO_PROCESS_ID and 
	//team.C_Pk_team_id = mapping.C_Pk_team_id and
	//team.C_PK_TEAM_ID =member.C_PK_TEAM_ID
 
 
 public void getRules(java.sql.Date startDate, java.sql.Date endDate, String frequency) throws SQLException{
	 
	 Connection con = null;
	 ArrayList<RuleEngineDTO> ruleEngineDToArray = new ArrayList<RuleEngineDTO>();
	 String SQLSTATEMENT = null;
	 Integer dayOfWeek=null;
	 if(frequency.equals(new String("Weekly")))
			 {
		 Calendar c = Calendar.getInstance();
		 c.setTime(startDate);
		 dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		 System.out.println("dayOfWeek -------"+dayOfWeek);
		 
		 SQLSTATEMENT= "select distinct team.C_Pk_team_id ,team.c_pk_epmo_process_id ,mapping.c_dp_derived_datapoint_alias,mapping.C_DP_RULE_ID, member.C_PK_EMP_ID "
                 + "from t_dp_epmo_process_rule_mapping mapping, T_DP_Team team, T_DP_TEAM_MEMBERS member, T_DP_Rules rule "
	 		+ "where "
	 		+ "team.c_pk_epmo_process_id= mapping.c_dp_epmo_process_id and "
	 		+ "mapping.C_DP_RULE_ID =rule.C_DP_RULE_ID and "
	 		+ "team.C_PK_TEAM_ID =member.C_PK_TEAM_ID and "
	 		+ "team.c_pk_status='active' and "
	 		+ "member.c_pk_allocation_start_date<=? and "
	 		+" ( member.c_pk_allocation_end_date>=?  OR member.c_pk_allocation_end_date IS NULL) and "
	 		+ "c_dp_calculation_frequency = ? and "
	 		+ "team.c_dp_week_start_day =? "
	 		+ "ORDER BY team.C_Pk_team_id";
			 }
	 else {
	 
	  SQLSTATEMENT= "select distinct team.C_Pk_team_id ,team.c_pk_epmo_process_id ,mapping.c_dp_derived_datapoint_alias,mapping.C_DP_RULE_ID, member.C_PK_EMP_ID "
                   + "from t_dp_epmo_process_rule_mapping mapping, T_DP_Team team, T_DP_TEAM_MEMBERS member, T_DP_Rules rule "
	 		+ "where "
	 		+ "team.c_pk_epmo_process_id= mapping.c_dp_epmo_process_id and "
	 		+ "mapping.C_DP_RULE_ID =rule.C_DP_RULE_ID and "
	 		+ "team.C_PK_TEAM_ID =member.C_PK_TEAM_ID and "
	 		+ "team.c_pk_status='active' and "
	 		+ "member.c_pk_allocation_start_date<=? and "
	 		+" ( member.c_pk_allocation_end_date>=?  OR member.c_pk_allocation_end_date IS NULL) and "
	 		+ "c_dp_calculation_frequency = ? " 
	 		+ "ORDER BY team.C_Pk_team_id";
	 }
		 
	 RuleEngineDTO ruleEngineDto = null;
	
	 try {
	 con = DatabbaseHelper.getInstance().getConnection();
	 PreparedStatement preparedStatement = con.prepareStatement(SQLSTATEMENT);
	 preparedStatement.setDate(1, endDate);
	 preparedStatement.setDate(2, startDate);
	 preparedStatement.setString(3, frequency);
	 if(frequency.equals(new String("Weekly")))
	  preparedStatement.setInt(4, dayOfWeek);
	 
	 ResultSet rs = preparedStatement.executeQuery();
	 con.setAutoCommit(false);
	 while ( rs.next() ){
		 ruleEngineDto = new RuleEngineDTO();
		 ruleEngineDto.setC_Pk_team_id(rs.getInt("C_Pk_team_id"));
		 ruleEngineDto.setC_PK_EPMO_PROCESS_ID(rs.getInt("c_pk_epmo_process_id"));
		 ruleEngineDto.setC_PK_EMP_ID(rs.getInt("C_PK_EMP_ID"));
		 ruleEngineDto.setC_DP_RULE_ID(rs.getInt("C_DP_RULE_ID"));
		 ruleEngineDto.setStartDate(startDate);
		 ruleEngineDto.setEndDate(endDate);
		 ruleEngineDto.setFrequency(frequency);
		 //ruleEngineDto.setProcessedDate(rs.getDate("ProcessedDate"));
		 ruleEngineDto.setC_DP_Derived_Datapoint_Alias(rs.getString("c_dp_derived_datapoint_alias"));
		 ruleEngineDToArray.add(ruleEngineDto);
       }
	 ExcecuteRule_updated(ruleEngineDToArray,con);
    }
 
	 catch (SQLException se)
	  {
	  System.out.println(se.toString());
	  }
	 
 }
 //Loop through each Rule_ID get Rule Details from T_DP_RULES Table
 //Get Rule Parameter for the Rule Selected from T_DP_RULE_PARAMETERS table.
 
 public void getRules(String dataPointAlias, int empId, int teamId, int processID, java.sql.Date startDate, java.sql.Date endDate, String frequency) throws SQLException{
	 
	 Connection con = null;
	 ArrayList<RuleEngineDTO> ruleEngineDToArray = new ArrayList<RuleEngineDTO>();
	 try { 
	 
	 String SQLSTATEMENT= "select team.C_Pk_team_id ,team.c_pk_epmo_process_id ,mapping.c_dp_derived_datapoint_alias,mapping.C_DP_RULE_ID, member.C_PK_EMP_ID "
                   + "from t_dp_epmo_process_rule_mapping mapping, T_DP_Team team, T_DP_TEAM_MEMBERS member, T_DP_Rules rule "
	 		+ "where "
	 		+ "team.c_pk_epmo_process_id= mapping.c_dp_epmo_process_id and "
	 		+ "mapping.C_DP_RULE_ID =rule.C_DP_RULE_ID and "
	 		+ "team.C_PK_TEAM_ID =member.C_PK_TEAM_ID and "
	 		+ "team.c_pk_status='active' and "
	 		+ "member.c_pk_allocation_start_date<=? and "
	 		+" ( member.c_pk_allocation_end_date>=?  OR member.c_pk_allocation_end_date IS NULL) and "
	 	    + "member.c_pk_emp_id =? and "
	 		+ "team.C_Pk_team_id =? and "
	 		+ "mapping.c_dp_epmo_process_id=? and "
	 		+ "(mapping.c_dp_derived_datapoint_alias =?  or rule.c_dp_derived_datapoint= ?) and "
	 		+ "rule.c_dp_applicable_frequency= ? "
	 		+ "ORDER BY team.C_Pk_team_id";
	 
	 	
	 RuleEngineDTO ruleEngineDto = null;
	
	
	 con = DatabbaseHelper.getInstance().getConnection();
	 PreparedStatement preparedStatement = con.prepareStatement(SQLSTATEMENT);
	 preparedStatement.setDate(1, startDate);
	 preparedStatement.setDate(2, endDate);
	 preparedStatement.setInt(3, empId);
	 preparedStatement.setInt(4, teamId);
	 preparedStatement.setInt(5, processID);
	 preparedStatement.setString(6, dataPointAlias);
	 preparedStatement.setString(7, dataPointAlias);
     preparedStatement.setString(8, frequency);
	 ResultSet rs = preparedStatement.executeQuery();
	 con.setAutoCommit(false);
	 while ( rs.next() ){
		 ruleEngineDto = new RuleEngineDTO();
		 ruleEngineDto.setC_Pk_team_id(rs.getInt("C_Pk_team_id"));
		 ruleEngineDto.setC_PK_EPMO_PROCESS_ID(rs.getInt("c_pk_epmo_process_id"));
		 ruleEngineDto.setC_PK_EMP_ID(rs.getInt("C_PK_EMP_ID"));
		 ruleEngineDto.setC_DP_RULE_ID(rs.getInt("c_DP_RULE_ID"));
		 //ruleEngineDto.setProcessedDate(rs.getDate("ProcessedDate"));
		 ruleEngineDto.setStartDate(startDate);
		 ruleEngineDto.setEndDate(endDate);
		 ruleEngineDto.setC_DP_Derived_Datapoint_Alias(rs.getString("c_dp_derived_datapoint_alias"));
		 ruleEngineDToArray.add(ruleEngineDto);
       }
	 ExcecuteRule_updated(ruleEngineDToArray,con);
      }
	catch (SQLException se)
	  {
	  System.out.println(se.toString());
	  }
   }

public void ExcecuteRule_updated( ArrayList<RuleEngineDTO> ruleEngineDToArray, Connection con) throws SQLException{
	 
	 RuleEngineDTO ruleEngineDto;
	 TDpRuleParametersDTO rulesParameter;
	 PreparedStatement ps =null;
	 PreparedStatement preparedStatementRuleQuery =null;
	 PreparedStatement preparedStatement=null ;
	 PreparedStatement preparedStatementRule=null;
	 ArrayList<TDpRuleParametersDTO> rulesParameterArray = new ArrayList<TDpRuleParametersDTO>();
	 String valueDataPoint =null;
	 Date StartDate =null;
	 Date EndDate=null;
	 Integer oldProcessId=null;
	 Integer newProcessId=null;
	 try {
		 String insertInRuleEvaluationData = "insert into t_dp_rule_evaluation_data values(?,?,?,?,?,?,?,?)";
		 ps = con.prepareStatement(insertInRuleEvaluationData);
		 if(ruleEngineDToArray.size()>1) {
			 ruleEngineDto = ruleEngineDToArray.get(0); 
			 oldProcessId=ruleEngineDto.getC_PK_EPMO_PROCESS_ID();
		 }
	 for(int i=0;i<ruleEngineDToArray.size();i++) {
		 ruleEngineDto = ruleEngineDToArray.get(i);
		 int ruleID = ruleEngineDto.getC_DP_RULE_ID();
		 Map<String,Object> map=new HashMap<String,Object>();
		 map.put("EmpId", ruleEngineDto.getC_PK_EMP_ID());
		 map.put("ProcessedDate", ruleEngineDto.getStartDate());
		 map.put("ProcessId", ruleEngineDto.getC_PK_EPMO_PROCESS_ID());
		 map.put("StartDate", ruleEngineDto.getStartDate());
		 map.put("EndDate", ruleEngineDto.getEndDate());
		 map.put("ProcessStartDate", ruleEngineDto.getStartDate());
		 map.put("ProcessEndDate", ruleEngineDto.getEndDate());
		 map.put("TeamId", ruleEngineDto.getC_Pk_team_id());
		 StartDate =ruleEngineDto.getStartDate();
		 EndDate =ruleEngineDto.getEndDate();
			 
		 
         //Check Data Exist in Rule evaluation Table exist or not.
		 
		 String SQLSTATEMENTRuleEvalation ="Select evaluation.c_dp_derived_data_value from t_dp_rule_evaluation_data evaluation where "
		 		+ "evaluation.c_dp_rule_id =? and "
		 		+ "evaluation.c_dp_process_id =? and "
		 		+ "evaluation.c_dp_emp_id =? and "
		 		+ "evaluation.c_dp_team_id =? and "
		 		+ "evaluation.c_dp_start_date =? and "
		 		+ "evaluation.c_dp_end_date =?";
		 
		 preparedStatement = con.prepareStatement(SQLSTATEMENTRuleEvalation);
		 preparedStatement.setInt(1, ruleID);
		 preparedStatement.setInt(2,ruleEngineDto.getC_PK_EPMO_PROCESS_ID());
		 preparedStatement.setInt(3,ruleEngineDto.getC_PK_EMP_ID());
		 preparedStatement.setInt(4,ruleEngineDto.getC_Pk_team_id());
		 preparedStatement.setDate(5,ruleEngineDto.getStartDate());
		 preparedStatement.setDate(6,ruleEngineDto.getEndDate());
		 ResultSet rsEvalution = preparedStatement.executeQuery();
		 
		 while(rsEvalution.next())
			   valueDataPoint =rsEvalution.getString("c_dp_derived_data_value");
		  if(valueDataPoint!=null)
		  {
			 System.out.println("Data Already Exist for this");
			 continue; 
		  }
		 
		  //Rule_ID get Rule Details from T_DP_RULES Table
		 String SQLSTATEMENT= "select * from T_DP_RULES where C_DP_RULE_ID = ?";
		 String SQLSTATEMENTRuleDetails= "select * from T_DP_RULE_PARAMETERS where C_DP_RULE_ID = ?";
		 TDpRulesDTO ruleDto = null;
	     preparedStatement = con.prepareStatement(SQLSTATEMENT);
		 preparedStatement.setInt(1, ruleID);
		 ResultSet rs = preparedStatement.executeQuery();
		 TDpRulesDTO rules =new TDpRulesDTO();
		  while ( rs.next() ) {
			 rules = new TDpRulesDTO();
			 rules.setRule_ID(rs.getInt("C_DP_RULE_ID"));
			 rules.setRule_Query(rs.getString("C_DP_RULE_QUERY"));
			 rules.setApplicability(rs.getString("C_DP_APPLICABLE_FREQUENCY"));
			 rules.setApplicable_Freq(rs.getString("C_DP_APPLICABLE_FREQUENCY"));
			 rules.setDerived_Data_Point(rs.getString("C_DP_DERIVED_DATAPOINT"));
			 rules.setDerived_Datapoint_Algorithm(rs.getString("C_DP_DERIVED_DATAPOINT_ALGORITHM"));
			 rules.setDerived_Datapoint_Description(rs.getString("C_DP_DERIVED_DATAPOINT_DESCRIPTION"));
			 }
		  preparedStatementRule = con.prepareStatement(SQLSTATEMENTRuleDetails);
		  preparedStatementRule.setInt(1, ruleID);
		  ResultSet rsRuleDetails = preparedStatementRule.executeQuery();
		    
			//Get Rule Paramater for the Rule Selected from T_DP_RULE_PARAMETERS table
			 Map<Integer,Object> mapParameter=new HashMap<Integer,Object>();
			 while ( rsRuleDetails.next() )  {
				 rulesParameter = new TDpRuleParametersDTO();
				 rulesParameter.setRule_ID(rsRuleDetails.getInt("C_DP_RULE_ID"));
				 rulesParameter.setParameter_Name(rsRuleDetails.getString("C_DP_PARAMETER_NAME"));
				 rulesParameter.setParameter_Sequence(rsRuleDetails.getString("C_DP_PARAMETER_SEQUENCE"));
				 rulesParameter.setParameter_Query(rsRuleDetails.getString("C_DP_PARAMETER_QUERY"));
				 rulesParameter.setParameter_Type(rsRuleDetails.getString("C_DP_PARAMETER_TYPE"));
				 for (Map.Entry<String,Object> entry : map.entrySet()){
				     //System.out.println(entry.getKey() + "/" + entry.getValue());
				     //System.out.println("Parameter Name" + rsRuleDetails.getString("C_DP_PARAMETER_NAME"));
				     if(entry.getKey().equals(rulesParameter.getParameter_Name()))
				       mapParameter.put(new Integer(rulesParameter.getParameter_Sequence()), entry.getValue());
				   
				 }
			  rulesParameterArray.add(rulesParameter);
			  }
			 String ruleQuery = rules.getRule_Query();
			 preparedStatementRuleQuery = con.prepareStatement(ruleQuery);
			 System.out.println("ruleQuery" + ruleQuery);
			 for (Map.Entry<Integer,Object> entry : mapParameter.entrySet()) {
			 System.out.println(entry.getKey() + "/parameter" + entry.getValue());
			 preparedStatementRuleQuery.setObject(entry.getKey() ,entry.getValue());
			 }
			 ResultSet rsResult = preparedStatementRuleQuery.executeQuery();
			 
			 while ( rsResult.next()) {
				  String value = rsResult.getString("value");
				  float valueData=0;
				  if(value!=null)
					  valueData =Float.parseFloat(value) ; 
				  System.out.println("Rule Id:" +ruleEngineDto.getC_DP_RULE_ID()+ " processId :"+
				  ruleEngineDto.getC_PK_EPMO_PROCESS_ID()+" Team Id"+ruleEngineDto.getC_Pk_team_id()+" Emp Id :"+
				  ruleEngineDto.getC_PK_EMP_ID()+"Derived Dataelement Name :"+ruleEngineDto.getC_DP_Derived_Datapoint_Alias()+
				  "Derived Dataelement value :"+value +" Start date " + ruleEngineDto.getProcessedDate()+" End date " + ruleEngineDto.getProcessedDate());
				  
				  
				  ps.setInt(1,ruleEngineDto.getC_DP_RULE_ID());
				  ps.setInt(2,ruleEngineDto.getC_PK_EPMO_PROCESS_ID());
				  ps.setInt(3, ruleEngineDto.getC_Pk_team_id());
				  ps.setInt(4,ruleEngineDto.getC_PK_EMP_ID());
				  ps.setString(5,rules.getDerived_Data_Point());
				  ps.setFloat(6,valueData);
				  ps.setDate(7,StartDate);
				  ps.setDate(8,EndDate);
				  newProcessId=ruleEngineDto.getC_PK_EPMO_PROCESS_ID();
				  ps.addBatch();
				  if(oldProcessId!=newProcessId)
				  {
					 ps.executeBatch();
				     con.commit();
				     System.out.println("Data Saved for ProcessIds"+oldProcessId);
				     oldProcessId=newProcessId;
				     ps.clearBatch();
				  }
				 
				  }
			
		         }
	 if(ps!=null) {
		 System.out.println("lastProcess Ids"+oldProcessId);
      ps.executeBatch();
	  con.commit();
	  ps.close();
	  }  
	 }
 catch (SQLException se) {
 System.out.println(se.toString());
  }
 DatabbaseHelper.getInstance().closeConnection();
 }
 
}
