package com.empowerment.GoalScoreCalc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

//import org.apache.log4j.Logger;

import com.empowerment.util.DatabbaseHelper;


public class DailyGoalScoreCalcService {
	//final static Logger logger = Logger.getLogger(DailyGoalScoreCalcService.class);
	GoalScoreCalcDao goalScoreCalcDao =null;
	Connection con=null;
	String SQLSTATEMENT=null;
	DailyGoalScoreCalcService() throws ClassNotFoundException
	{
		try
		 {
			 goalScoreCalcDao = new GoalScoreCalcDao();
			 this.con= DatabbaseHelper.getInstance().getConnection();
			 
			 
			 String SQLGOALTABLE = "SELECT g.c_pk_goal_id,"
			 		+ " g.c_pk_emp_id,"
			 		+ " g.c_pk_goal_frequency_of_measurement,"
			 		+ " g.c_pk_goal_calculation_method,"
			 		+ " e.c_dp_start_date,"
			 		+ " e.c_dp_end_date"
			 		
			 		+ " FROM t_dp_goal g,"
			 		+ " t_dp_goal_expected_data e"
			 		+ "	WHERE"
			 		+ " g.c_pk_goal_id = e.c_pk_goal_id"
			 		+ " AND  g.c_pk_goal_calculation_method = 'Auto'";
			 
			 PreparedStatement preparedStatement =null;
			 preparedStatement = con.prepareStatement(SQLGOALTABLE);
			 ResultSet rs1=preparedStatement.executeQuery();
			 ActualDataValueDto actualDataValueDto  = new ActualDataValueDto();
			 ArrayList<ActualDataValueDto> actualDataValueDtoArray = new ArrayList<ActualDataValueDto>();
			 while(rs1.next())
			 {	
				  actualDataValueDto  = new ActualDataValueDto();
				  actualDataValueDto.setC_pk_goal_id(rs1.getInt("C_PK_GOAL_ID"));
				  actualDataValueDto.setC_pk_emp_id(rs1.getInt("C_PK_EMP_ID"));
				  actualDataValueDto.setC_pk_goal_frequency_of_measurement(rs1.getString("c_pk_goal_frequency_of_measurement"));
				  actualDataValueDto.setC_pk_goal_calculation_method(rs1.getString("c_pk_goal_calculation_method"));
				  actualDataValueDto.setC_dp_start_date(rs1.getString("c_dp_start_date"));
				  actualDataValueDto.setC_dp_end_date(rs1.getString("c_dp_end_date"));
				  actualDataValueDtoArray.add(actualDataValueDto);
			 }
			 
			 for(int i=0; i<actualDataValueDtoArray.size(); i++)
			 {
				 AchievedValueMethodDao achievedValueMethodDao = new AchievedValueMethodDao();
				 if(actualDataValueDto.getC_pk_goal_calculation_method().equals("Auto"))
				 {  
				     int achievedvalue = achievedValueMethodDao.getAchievedValue(actualDataValueDto.getC_pk_goal_id(), actualDataValueDto.getC_dp_start_date(), actualDataValueDto.getC_dp_end_date());
				     
				     String UPDATEGOALTABLE = "Update t_dp_goal_expected_data set c_dp_goal_achieved_value="+achievedvalue+" where C_PK_GOAL_ID=?";
					 preparedStatement = con.prepareStatement(UPDATEGOALTABLE);
					 preparedStatement.setInt(1,actualDataValueDto.getC_pk_goal_id());
					 preparedStatement.executeUpdate();
				 }
			 } 
				 
				 
			 //////////////
			 this.SQLSTATEMENT= "SELECT "
					 + " e.c_pk_goal_id,"
					 + " e.c_dp_emp_id,"
					 + " e.c_pk_team_id,"
					 + " e.c_dp_start_date,"
					 + " e.c_dp_end_date, "
					 + " e.c_dp_goal_achieved_value,"
					 + " e.c_dp_num_working_days, "
					 + " e.c_dp_data_source, "
					 + " g.c_pk_section_weightage,"
					 + " g.c_pk_goal_frequency_of_measurement, "
					 + " g.c_pk_proration_allowed, "
					 + " g.c_pk_goal_measurement_direction,"
					 + " g.c_pk_section_name,"
					 + " g.c_pk_section_type,"
					 + " s.c_dp_subsection_weightage,"
					 + " g.c_pk_goal_calculation_method,"
					 + " w.c_dp_goal_weightage, "
					 + " t.c_dp_goal_target,"
					 + " m.c_pk_allocation_percentage, "
					 + " m.c_pk_peak_role,"
					 + " d.c_dp_num_working_days"                                 
					 
					 + " FROM "
					 + " t_dp_goal g,"
					 + " t_dp_goal_expected_data e,"
					 + " t_dp_subsection_weightage s, "
					 + " t_dp_goal_weightage w,"
					 + " t_dp_goal_target t, "
					 + " t_dp_team_members m,"
					 + " t_dp_team_workingdays d"                                  
					 
					 + " WHERE "
					 + " e.c_pk_goal_id = g.c_pk_goal_id"
					 + " AND e.c_dp_start_date =?"
					 + " AND e.c_dp_end_date =?"
					 + " AND UPPER(g.c_pk_goal_frequency_of_measurement) =?"
					 
					 + " AND g.c_pk_sub_section_id= s.c_pk_sub_section_id"
					 + " AND s.c_dp_effective_start_date <= ?"
					 + " AND (s.c_dp_effective_end_date>=? OR s.c_dp_effective_end_date is null)"
					 
					 + " AND g.c_pk_goal_id = w.c_pk_goal_id"
					 + " AND w.c_dp_effective_start_date <=?"
					 + " AND (w.c_dp_effective_end_date>=? OR w.c_dp_effective_end_date is null)"

					 + " AND g.c_pk_goal_id = t.c_pk_goal_id"
					 + " AND t.c_dp_effective_start_date <=?"
					 + " AND (t.c_dp_effective_end_date>=? OR t.c_dp_effective_end_date is null)"
					 
					 + " AND g.c_pk_team_id = m.c_pk_team_id"
					 + " AND G.C_PK_EMP_ID = M.C_PK_EMP_ID"
					 + " AND M.C_PK_ALLOCATION_START_DATE <=?"
					 + " AND (m.c_pk_allocation_end_date>=? OR m.c_pk_allocation_end_date is null)"
					 
                     + " AND g.c_pk_team_id = d.c_pk_team_id"                                     
					 
			         + " AND e.c_dp_goal_achieved_value IS NOT NULL"; 
			 
		 } 
		 catch (SQLException e) 
		 {
			   // logger.error("Goal Score Calc Service - SQLException... Error!!!", e);
				System.out.println("Goal Score Calc Service - SQLException "+ e);
		 }	
	}
	
	//Method to calculate Daily score
	public void dailyRecords(Date inputdate)
	{   
		PreparedStatement preparedStatement =null;
		try {
			preparedStatement = con.prepareStatement(this.SQLSTATEMENT);
			preparedStatement.setDate(1, inputdate);
			preparedStatement.setDate(2, inputdate);
			preparedStatement.setString(3, "DAILY");
			preparedStatement.setDate(4, inputdate);
			preparedStatement.setDate(5, inputdate);
			preparedStatement.setDate(6, inputdate);
			preparedStatement.setDate(7, inputdate);
			preparedStatement.setDate(8, inputdate);
			preparedStatement.setDate(9, inputdate);
			preparedStatement.setDate(10, inputdate);
			preparedStatement.setDate(11, inputdate);
			
			insertRecordIntoGoalScoreCalc(preparedStatement);
			
		} catch (SQLException | ParseException e) {
			System.out.println("DailyGoalScoreCalcService - Exception " + e);
		}
	}
	
	//Method to calculate Monthly score
	public void monthlyRecords(int month, int year)
	{
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 Calendar start_date  = new GregorianCalendar(year, month-1, 1);
		 Date calMonthStartDate=new java.sql.Date(start_date.getTime().getTime());
		
		 int daysInMonth = start_date.getActualMaximum(Calendar.DAY_OF_MONTH);
		 Calendar end_date = new GregorianCalendar(year, month-1,daysInMonth);
		 Date calMonthEndDate=new java.sql.Date(end_date.getTime().getTime());
		
		PreparedStatement preparedStatement =null;
		try {
			preparedStatement = con.prepareStatement(this.SQLSTATEMENT);
			preparedStatement.setDate(1, calMonthStartDate);
			preparedStatement.setDate(2, calMonthEndDate);
			preparedStatement.setString(3, "MONTHLY");
			preparedStatement.setDate(4, calMonthStartDate);
			preparedStatement.setDate(5, calMonthEndDate);
			preparedStatement.setDate(6, calMonthStartDate);
			preparedStatement.setDate(7, calMonthEndDate);
			preparedStatement.setDate(8, calMonthStartDate);
			preparedStatement.setDate(9, calMonthEndDate);
			preparedStatement.setDate(10, calMonthStartDate);
			preparedStatement.setDate(11, calMonthEndDate);
			
			insertRecordIntoGoalScoreCalc(preparedStatement);
			
			
		} catch (SQLException | ParseException e) {
			System.out.println("DailyGoalScoreCalcService - Exception " + e);
		}
	}
	
	//Method to calculate Weekly score
	public void weeklyRecords(Date weekStartDate, Date weekEndDate)
	{
		
		    PreparedStatement preparedStatement =null;
		    try {
			preparedStatement = con.prepareStatement(this.SQLSTATEMENT);
			preparedStatement.setDate(1, weekStartDate);
			preparedStatement.setDate(2, weekEndDate);
			preparedStatement.setString(3, "WEEKLY");
			preparedStatement.setDate(4, weekStartDate);
			preparedStatement.setDate(5, weekEndDate);
			preparedStatement.setDate(6, weekStartDate);
			preparedStatement.setDate(7, weekEndDate);
			preparedStatement.setDate(8, weekStartDate);
			preparedStatement.setDate(9, weekEndDate);
			preparedStatement.setDate(10, weekStartDate);
			preparedStatement.setDate(11, weekEndDate);
			
			insertRecordIntoGoalScoreCalc(preparedStatement);
			
			
		} catch (SQLException | ParseException e) {
			System.out.println("DailyGoalScoreCalcService - Exception " + e);
		}
	}
	
	
	 public void insertRecordIntoGoalScoreCalc(PreparedStatement preparedStatement) throws ParseException 
	 { 
		 ArrayList<ExpectedGoalDto> expectedGoalArray = new ArrayList<ExpectedGoalDto>();
	     Calendar cal = Calendar.getInstance();
		 Date scrCalDate = new Date(cal.getTime().getTime());
		// System.out.println("Calender date - "+scrCalDate);
	
    	 ResultSet rs= null;
 
		 try
		 {
			 rs=preparedStatement.executeQuery();
			 ExpectedGoalDto expectedGoalDto =new ExpectedGoalDto();
			 while(rs.next())
			 {	
				 expectedGoalDto =new ExpectedGoalDto();
				 expectedGoalDto.setGoalID(rs.getInt("C_PK_GOAL_ID"));
				 expectedGoalDto.setEmpID(rs.getInt("C_DP_EMP_ID"));
				 expectedGoalDto.setTeamID(rs.getInt("C_PK_TEAM_ID"));
				 expectedGoalDto.setStartDate(rs.getDate("C_DP_START_DATE"));
				 expectedGoalDto.setEndDate(rs.getDate("C_DP_END_DATE"));
				 expectedGoalDto.setGoal_achieved_value(rs.getDouble("C_DP_GOAL_ACHIEVED_VALUE"));
				 expectedGoalDto.setNo_of_working_days(rs.getInt("C_DP_NUM_WORKING_DAYS"));
				 expectedGoalDto.setDataSource(rs.getString("C_DP_DATA_SOURCE"));
				 expectedGoalDto.setSection_weitage(rs.getDouble("C_PK_SECTION_WEIGHTAGE"));
				 expectedGoalDto.setSectionName(rs.getString("C_PK_SECTION_NAME"));
				 expectedGoalDto.setGoal_frequency_of_measurement(rs.getString("C_PK_GOAL_FREQUENCY_OF_MEASUREMENT"));
				 expectedGoalDto.setProration_allowed(rs.getBoolean("C_PK_PRORATION_ALLOWED"));
				 expectedGoalDto.setMeasurement_direction(rs.getString("C_PK_GOAL_MEASUREMENT_DIRECTION"));
				 expectedGoalDto.setGoalTarget(rs.getDouble("C_DP_GOAL_TARGET"));
				 expectedGoalDto.setGoalWeightage(rs.getDouble("C_DP_GOAL_WEIGHTAGE"));
				 expectedGoalDto.setSubSectionWeitage(rs.getDouble("C_DP_SUBSECTION_WEIGHTAGE"));
				 expectedGoalDto.setAllocationPerc(rs.getDouble("C_PK_ALLOCATION_PERCENTAGE"));
				 expectedGoalDto.setPeakRole(rs.getString("C_PK_PEAK_ROLE"));
				 expectedGoalDto.setCalculationMethod(rs.getString("C_PK_GOAL_CALCULATION_METHOD"));
				 expectedGoalDto.setSectionType(rs.getString("C_PK_SECTION_TYPE"));
				 expectedGoalDto.setNo_Of_Working_Days_In_Period(rs.getInt("C_DP_NUM_WORKING_DAYS"));                  
			 
				 expectedGoalArray.add(expectedGoalDto);
			 }
					 
			 List<CalScoreDto> calScoreDtolst = new ArrayList<CalScoreDto>();
			
			//iterate each Expected Arraylist from the Arraylist and get the emp id
			 for(int i=0; i<expectedGoalArray.size(); i++)
			 {
				  expectedGoalDto = expectedGoalArray.get(i);
				 CalScoreDto calscoredto = new CalScoreDto();
				 int goalScoreCalcGoalId  = expectedGoalDto.getGoalID();
				 int goalScoreCalcEmpId  = expectedGoalDto.getEmpID();
				 int goalScoreCalcTeamId  = expectedGoalDto.getTeamID();
				 Date goalScoreCalcStartdate  = expectedGoalDto.getStartDate();
				 Date goalScoreCalcEnddate  = expectedGoalDto.getEndDate();
				 Double goalScoreCalcGoalAchievedvalue  = expectedGoalDto.getGoal_achieved_value();
				 int goalScoreCalcNoOfWorkingdays = expectedGoalDto.getNo_of_working_days();
				 String goalScoreCalcDataSource = expectedGoalDto.getDataSource();
				 Date goalScoreCalcScoreCalcDate = scrCalDate;
				 Double goalScoreCalcsectionWeightage  = expectedGoalDto.getSection_weitage();
				 String goalScoreCalcFreqOfMeaserment  = expectedGoalDto.getGoal_frequency_of_measurement();
				 Boolean goalScoreCalcProrationAllowed  = expectedGoalDto.isProration_allowed();
				 String goalScoreCalcMeasurmentDirection  = expectedGoalDto.getMeasurement_direction();
				 Double goalScoreCalcGoalWeightage = expectedGoalDto.getGoalWeightage();
				 Double goalScoreCalcGoalTarget = expectedGoalDto.getGoalTarget();
				 Double goalScoreCalcSubSectionWeightage = expectedGoalDto.getSubSectionWeitage();
				 Double goalScoreCalcAllocationPerc = expectedGoalDto.getAllocationPerc(); 
				 String goalScoreCalcPeakRole = expectedGoalDto.getPeakRole();
				 int countOfEmployee = expectedGoalDto.getCountOfEmpId();
				 Double sumOfAllocationPercentage = expectedGoalDto.getSumOfPercentageAlloc();
				 String goalScoreCalcsectionType=expectedGoalDto.getSectionType();
				 int goalScoreCalcNoOfWorkingDaysInPeriodMonth=expectedGoalDto.getNo_Of_Working_Days_In_Period();    
				 int goalScoreCalcNoOfWorkingDaysInPeriodWeek=5; 
				 
				 Date goalScoreCalcPerformancedate = goalScoreCalcStartdate;
	
				 
				 Double taskPerformanceScoreCalculation = null; 
				 Double goalScoreCalculation = null;
				 Double subSectionScoreCalculation=null;
				 Double sectionScoreCalculation=null;
				 Double teamScoreCalculation=null;
				 Double calculatedTarget=null;
				 
					 if(goalScoreCalcsectionType.equals("critical_measure"))
					 {
				         //calculate Target
						 if(goalScoreCalcProrationAllowed==false)
						 {
							 calculatedTarget = goalScoreCalcGoalTarget;
							 calscoredto.getCalAllocationPercList().add(calculatedTarget);
							 
							 calscoredto.setCal_Goal_achieved_value(goalScoreCalcGoalAchievedvalue);
							 
							 taskPerformanceScoreCalculation = CalculateTaskScore(goalScoreCalcGoalAchievedvalue, goalScoreCalcGoalTarget, goalScoreCalcMeasurmentDirection);
							 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
						 }
						 else 
						 {
							 if(goalScoreCalcFreqOfMeaserment.equals("Monthly"))
							 {
								 Double targetInMonthForEmp = goalScoreCalcGoalTarget/goalScoreCalcNoOfWorkingDaysInPeriodMonth;
								 Double allocationPercForEmp = goalScoreCalcAllocationPerc/goalScoreCalcNoOfWorkingDaysInPeriodMonth;
								 Double monthlyAchievedValueForEmp = goalScoreCalcGoalAchievedvalue/goalScoreCalcNoOfWorkingDaysInPeriodMonth; 
								 for(int j=0; j<=goalScoreCalcNoOfWorkingDaysInPeriodMonth;j++)
								 {
									 Double calculatedTarget2 =  CalculateMonthTarget(targetInMonthForEmp,allocationPercForEmp,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodMonth);
									 calscoredto.getCalAllocationPercList().add(calculatedTarget2);
								 }
								 
								 calscoredto.setCal_Goal_achieved_value(monthlyAchievedValueForEmp);
								 
								 taskPerformanceScoreCalculation = CalculateTaskScore(monthlyAchievedValueForEmp, targetInMonthForEmp, goalScoreCalcMeasurmentDirection);
								 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
								 
							 }
							 
							 else if(goalScoreCalcFreqOfMeaserment.equals("Weekly"))
							 {
								 Double targetInWeekForEmp = goalScoreCalcGoalTarget/goalScoreCalcNoOfWorkingDaysInPeriodWeek;
								 Double allocationPercForEmp = goalScoreCalcAllocationPerc/goalScoreCalcNoOfWorkingDaysInPeriodWeek;
								 Double weeklyachievedValueForEmp = goalScoreCalcGoalAchievedvalue/goalScoreCalcNoOfWorkingDaysInPeriodWeek; 
								 
								 for(int j=0; j<=goalScoreCalcNoOfWorkingDaysInPeriodWeek;j++)
								 {
									 Double  calculatedTarget1=  CalculateWeekTarget(targetInWeekForEmp,allocationPercForEmp,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodWeek);
								     calscoredto.getCalAllocationPercList().add(calculatedTarget1);
								 }
								 
								 calscoredto.setCal_Goal_achieved_value(weeklyachievedValueForEmp);
								 
								 taskPerformanceScoreCalculation = CalculateTaskScore(weeklyachievedValueForEmp, targetInWeekForEmp, goalScoreCalcMeasurmentDirection);
								 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
								 
							 }		 
							 else
							 {
								 calculatedTarget =  CalculateTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays);
								 calscoredto.getCalAllocationPercList().add(calculatedTarget);
								 
								 calscoredto.setCal_Goal_achieved_value(goalScoreCalcGoalAchievedvalue);
								 
								 taskPerformanceScoreCalculation = CalculateTaskScore(goalScoreCalcGoalAchievedvalue, goalScoreCalcGoalTarget, goalScoreCalcMeasurmentDirection);
								 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
								 
							 }
						 }
					 
						 //taskPerformanceScoreCalculation = CalculateTaskScore(goalScoreCalcGoalAchievedvalue, goalScoreCalcGoalTarget, goalScoreCalcMeasurmentDirection);
						   
					   
						 goalScoreCalculation = GoalScoreCalculation(taskPerformanceScoreCalculation,goalScoreCalcGoalWeightage);
	
					     subSectionScoreCalculation = SubSectionScoreCalculation(goalScoreCalculation , goalScoreCalcSubSectionWeightage);
					 
					     sectionScoreCalculation = SectionScoreCalculation(subSectionScoreCalculation,goalScoreCalcsectionWeightage);
					
					     teamScoreCalculation=TeamScoreCalculation(sectionScoreCalculation,goalScoreCalcAllocationPerc);
						 
						 calscoredto.setCalGoalID(goalScoreCalcGoalId);
						 calscoredto.setCalEmpID(goalScoreCalcEmpId);
						 calscoredto.setCalTeamID(goalScoreCalcTeamId);
						 calscoredto.setCalStartDate(goalScoreCalcStartdate);
						 calscoredto.setCalEndDate(goalScoreCalcEnddate);
						 //calscoredto.setCal_Goal_achieved_value(goalScoreCalcGoalAchievedvalue);
						 calscoredto.setCal_No_of_working_days(goalScoreCalcNoOfWorkingdays);
						 calscoredto.setCaldataSource(goalScoreCalcDataSource);
						 calscoredto.setCalScoreCalDate(goalScoreCalcScoreCalcDate);
						 calscoredto.setCal_section_weitage(goalScoreCalcsectionWeightage);
						 calscoredto.setCal_goal_frequency_of_measurement(goalScoreCalcFreqOfMeaserment);
						 calscoredto.setCal_proration_allowed(goalScoreCalcProrationAllowed);
	                 	 calscoredto.setCal_measurement_direction(goalScoreCalcMeasurmentDirection);
						 calscoredto.setCalgoalWeightage(goalScoreCalcGoalWeightage);
						 calscoredto.setCalgoalTarget(goalScoreCalcGoalTarget);
						 calscoredto.setCalsubSectionWeitage(goalScoreCalcSubSectionWeightage);
//						 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
						 calscoredto.setCalgoalScoreCalculation(goalScoreCalculation);
						 calscoredto.setCalsubSectionScoreCalculation(subSectionScoreCalculation);
						 calscoredto.setCalsectionScoreCalculation(sectionScoreCalculation);
						 calscoredto.setCalteamScoreCalculation(teamScoreCalculation);
						 calscoredto.setCalPerformanceDate(goalScoreCalcPerformancedate);
						 calscoredto.setCal_No_Of_Working_Days_In_Period(goalScoreCalcNoOfWorkingDaysInPeriodMonth);             
						 
						 calScoreDtolst.add(calscoredto);
					 }
					 
					 else if(goalScoreCalcsectionType.equals("Success Events"))
					 {
						 if(goalScoreCalcProrationAllowed==false)
						 {
							 calculatedTarget = goalScoreCalcGoalTarget;
							 //calscoredto.getCalAllocationPercList().add(calculatedTarget);
						 }
						 else 
						 {
							 if(goalScoreCalcFreqOfMeaserment.equals("Monthly"))
								 calculatedTarget =  CalculateMonthTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodMonth);
							 
							 else if(goalScoreCalcFreqOfMeaserment.equals("Weekly"))
								 calculatedTarget =  CalculateWeekTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodWeek);
								 
							 else
								 calculatedTarget =  CalculateTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays);
						 }
					 
				         taskPerformanceScoreCalculation = CalculateTaskScore(goalScoreCalcGoalAchievedvalue, goalScoreCalcGoalTarget, goalScoreCalcMeasurmentDirection);
				   
					     goalScoreCalculation = GoalScoreCalculation(taskPerformanceScoreCalculation,goalScoreCalcGoalWeightage);
	
					     subSectionScoreCalculation = SubSectionScoreCalculation(goalScoreCalculation , goalScoreCalcSubSectionWeightage);
					 
					     sectionScoreCalculation = SectionScoreCalculation(subSectionScoreCalculation,goalScoreCalcsectionWeightage);
					
					     teamScoreCalculation=TeamScoreCalculation(sectionScoreCalculation,goalScoreCalcAllocationPerc);
							 
						 calscoredto.setCalGoalID(goalScoreCalcGoalId);
						 calscoredto.setCalEmpID(goalScoreCalcEmpId);
						 calscoredto.setCalTeamID(goalScoreCalcTeamId);
						 calscoredto.setCalStartDate(goalScoreCalcStartdate);
						 calscoredto.setCalEndDate(goalScoreCalcEnddate);
						 calscoredto.setCal_Goal_achieved_value(goalScoreCalcGoalAchievedvalue);
						 calscoredto.setCal_No_of_working_days(goalScoreCalcNoOfWorkingdays);
						 calscoredto.setCaldataSource(goalScoreCalcDataSource);
						 calscoredto.setCalScoreCalDate(goalScoreCalcScoreCalcDate);
						 calscoredto.setCal_section_weitage(goalScoreCalcsectionWeightage);
						 calscoredto.setCal_goal_frequency_of_measurement(goalScoreCalcFreqOfMeaserment);
						 calscoredto.setCal_proration_allowed(goalScoreCalcProrationAllowed);
	                 	 calscoredto.setCal_measurement_direction(goalScoreCalcMeasurmentDirection);
						 calscoredto.setCalgoalWeightage(goalScoreCalcGoalWeightage);
						 calscoredto.setCalgoalTarget(goalScoreCalcGoalTarget);
						 calscoredto.getCalAllocationPercList().add(calculatedTarget); 
						 calscoredto.setCalsubSectionWeitage(goalScoreCalcSubSectionWeightage);
						 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
						 calscoredto.setCalgoalScoreCalculation(goalScoreCalculation);
						 calscoredto.setCalsubSectionScoreCalculation(subSectionScoreCalculation);
						 calscoredto.setCalsectionScoreCalculation(sectionScoreCalculation);
						 calscoredto.setCalteamScoreCalculation(teamScoreCalculation);
						 calscoredto.setCal_No_Of_Working_Days_In_Period(goalScoreCalcNoOfWorkingDaysInPeriodMonth);    
						 
						 calScoreDtolst.add(calscoredto);
					 }
					 else if(goalScoreCalcsectionType.equals("People Developement"))
					 {
						 if(goalScoreCalcProrationAllowed==false)
						 {
							 calculatedTarget = goalScoreCalcGoalTarget;
							 //calscoredto.getCalAllocationPercList().add(calculatedTarget);
						 }
						 else 
						 {
							 if(goalScoreCalcFreqOfMeaserment.equals("Monthly"))
								 calculatedTarget =  CalculateMonthTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodMonth);
							 
							 else if(goalScoreCalcFreqOfMeaserment.equals("Weekly"))
								 calculatedTarget =  CalculateWeekTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays,goalScoreCalcNoOfWorkingDaysInPeriodWeek);
								 
							 else
								 calculatedTarget =  CalculateTarget(goalScoreCalcGoalTarget,goalScoreCalcAllocationPerc,goalScoreCalcNoOfWorkingdays);
						 }
					 
				         taskPerformanceScoreCalculation = CalculateTaskScore(goalScoreCalcGoalAchievedvalue, goalScoreCalcGoalTarget, goalScoreCalcMeasurmentDirection);
				   
					     goalScoreCalculation = GoalScoreCalculation(taskPerformanceScoreCalculation,goalScoreCalcGoalWeightage);
	
					     subSectionScoreCalculation = SubSectionScoreCalculation(goalScoreCalculation , goalScoreCalcSubSectionWeightage);
					 
					     sectionScoreCalculation = SectionScoreCalculation(subSectionScoreCalculation,goalScoreCalcsectionWeightage);
					
					     teamScoreCalculation=TeamScoreCalculation(sectionScoreCalculation,goalScoreCalcAllocationPerc);
							 
						 calscoredto.setCalGoalID(goalScoreCalcGoalId);
						 calscoredto.setCalEmpID(goalScoreCalcEmpId);
						 calscoredto.setCalTeamID(goalScoreCalcTeamId);
						 calscoredto.setCalStartDate(goalScoreCalcStartdate);
						 calscoredto.setCalEndDate(goalScoreCalcEnddate);
						 calscoredto.setCal_Goal_achieved_value(goalScoreCalcGoalAchievedvalue);
						 calscoredto.setCal_No_of_working_days(goalScoreCalcNoOfWorkingdays);
						 calscoredto.setCaldataSource(goalScoreCalcDataSource);
						 calscoredto.setCalScoreCalDate(goalScoreCalcScoreCalcDate);
						 calscoredto.setCal_section_weitage(goalScoreCalcsectionWeightage);
						 calscoredto.setCal_goal_frequency_of_measurement(goalScoreCalcFreqOfMeaserment);
						 calscoredto.setCal_proration_allowed(goalScoreCalcProrationAllowed);
	                 	 calscoredto.setCal_measurement_direction(goalScoreCalcMeasurmentDirection);
						 calscoredto.setCalgoalWeightage(goalScoreCalcGoalWeightage);
						 calscoredto.setCalgoalTarget(goalScoreCalcGoalTarget);
						 calscoredto.getCalAllocationPercList().add(calculatedTarget); 
						 calscoredto.setCalsubSectionWeitage(goalScoreCalcSubSectionWeightage);
						 calscoredto.setCaltaskPerformanceScoreCalculation(taskPerformanceScoreCalculation);
						 calscoredto.setCalgoalScoreCalculation(goalScoreCalculation);
						 calscoredto.setCalsubSectionScoreCalculation(subSectionScoreCalculation);
						 calscoredto.setCalsectionScoreCalculation(sectionScoreCalculation);
						 calscoredto.setCalteamScoreCalculation(teamScoreCalculation);
						 calscoredto.setCal_No_Of_Working_Days_In_Period(goalScoreCalcNoOfWorkingDaysInPeriodMonth);    
						 
						 calScoreDtolst.add(calscoredto);
					 }
				 }
				// calling insertGoalscore method
				insertGoalscore(calScoreDtolst);  
		 }
		 catch (SQLException se) 
		 {
			 System.out.println("Goal Score Calc Service - SQLException1 "+ se);
		 }
	 }
	 
	 
	 /**
		 * This method is used to Insert the Daily score of the team member in the table
		 * @param calScoreDtolst
		 * @return void
		 * @throws SQLException
		 */
	 
	 
	 public void insertGoalscore(List calScoreDtolst) throws SQLException{

			PreparedStatement preparedStatement = null;	

			String SQLSTATEMENTGOAL = "INSERT INTO T_DP_DAILY_SCORE "
					+ " (C_PK_GOAL_ID,"
					+ " C_DP_PERFORMANCE_DATE,"
					+ " C_DP_DATA_SOURCE,"
					+ " C_DP_CALCULATED_TARGET, "
					+ " C_DP_GOAL_ACHIEVED_VALUE,"
					+ " C_DP_GOAL_PERCENTAGE_ACHIEVED_VALUE,"
					+ " C_DP_GOAL_SCORE,"
					+ " C_DP_SUB_SECTION_PERFORMANCE_SCORE, "
					+ " C_DP_SECTION_PERFORMANCE_SCORE,"
					+ " C_DP_TEAM_PERFORMANCE_SCORE,"
					+ " C_DP_CALCULATION_TIME)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?)";

			try
			{
			Iterator iterator = (Iterator) calScoreDtolst.iterator();
			while (iterator.hasNext()) {
				
				CalScoreDto calscoredto = (CalScoreDto) iterator.next();
				
				  int CalGoalID = calscoredto.getCalGoalID();
			      Date calPerformanceDate = calscoredto.getCalStartDate();
			      String calDataSource = calscoredto.getCaldataSource();
			      ArrayList<Double> calculatedTargetList = calscoredto.getCalAllocationPercList();
			      double CaltaskPerformanceScoreCalculation=calscoredto.getCaltaskPerformanceScoreCalculation();
			      double cal_Goal_achieved_value = calscoredto.getCal_Goal_achieved_value();
			 	  double CalgoalScoreCalculation=calscoredto.getCalgoalScoreCalculation();
				  double CalsubSectionScoreCalculation=calscoredto.getCalsubSectionScoreCalculation();
				  double CalsectionScoreCalculation=calscoredto.getCalsectionScoreCalculation();
				  double calteamScoreCalculation=calscoredto.getCalteamScoreCalculation();
				  Date calScoreCaldate = calscoredto.getCalScoreCalDate();
		
				  preparedStatement = con.prepareStatement(SQLSTATEMENTGOAL);
				  for(int i=0;i<calculatedTargetList.size();i++)
				  {
					  Calendar c = Calendar.getInstance();
					  c.setTime(calPerformanceDate);
					  c.add(Calendar.DATE, i);
					
					    preparedStatement.setInt(1, CalGoalID);   //c_pk_goal_id
						preparedStatement.setDate(2, new java.sql.Date(c.getTime().getTime()));   //c_dp_performance_date		
						preparedStatement.setString(3, calDataSource);   //c_dp_data_source
						preparedStatement.setDouble(4, calculatedTargetList.get(i));  //c_dp_calculated_target
						preparedStatement.setDouble(5, cal_Goal_achieved_value);  //c_dp_goal_achieved_value
						preparedStatement.setDouble(6, CaltaskPerformanceScoreCalculation);  //c_dp_goal_percentage_achieved_value
						preparedStatement.setDouble(7, CalgoalScoreCalculation);  //c_dp_goal_score
						preparedStatement.setDouble(8, CalsubSectionScoreCalculation); //c_dp_sub_section_performance_score
						preparedStatement.setDouble(9, CalsectionScoreCalculation);  //c_dp_section_performance_score
						preparedStatement.setDouble(10, calteamScoreCalculation);  //c_dp_team_performance_score
						preparedStatement.setDate(11, calScoreCaldate );  //c_dp_calculation_time

						preparedStatement.executeUpdate();  
				  }
				
				String SQLSTATEMENTGOALUPDATEPROCESSED="UPDATE T_DP_GOAL_EXPECTED_DATA  set C_DP_PROCESSED_DATE = ? where C_PK_GOAL_ID="+CalGoalID+"";
				
				preparedStatement = con.prepareStatement(SQLSTATEMENTGOALUPDATEPROCESSED);
				preparedStatement.setDate(1,calScoreCaldate);
				preparedStatement.executeUpdate();
		
			}
		}
		catch (SQLException se) 
		{
//				 System.out.println("Goal Score Calc Service - insertGoalscore  - SQLException2 "+ se);
				 System.out.println("\n Goal id already exist...\n\n");
		}
	 }
	 
	//Method to calculate Team Performance Score
	 public Double CalculateTaskScore(Double AchievedValue,Double TargetValue,String MeasurementDirection)
	 {
		 Double taskScore = null; 
		 if(TargetValue==0 && AchievedValue==0)
		 {
			 return 100.00;
		 }
		 else if(TargetValue==0)
		 {
			 return 0.00;
		 }
		 else if(AchievedValue==0)
		 {
			 return 0.00;
		 }
		 if(MeasurementDirection.equals(">=") || MeasurementDirection.equals(">")) 
		 {
			 taskScore= ((AchievedValue/TargetValue) * 100);
		 }
		 else if(MeasurementDirection.equals("<=") || MeasurementDirection.equals("<"))
		 {
			 taskScore=  ((((TargetValue - AchievedValue)/TargetValue) * 100) +100);
		 }
		 return taskScore;
	     }
	 	 
	     //Method to calculate Target WEEK
		 public Double CalculateWeekTarget(Double GoalTarget, Double AllocationPerc,int noOfWorkingDaysOfThePerson,int goalScoreCalcNoOfWorkingDaysInPeriodWeek)
         {   
             Double calculateTarget=null;
             calculateTarget = (((GoalTarget/goalScoreCalcNoOfWorkingDaysInPeriodWeek)*noOfWorkingDaysOfThePerson)*AllocationPerc);
             
             return calculateTarget;
         }
		 
		//Method to calculate Target MONTH
		 public Double CalculateMonthTarget(Double GoalTarget,Double AllocationPerc,int noOfWorkingDaysOfThePerson,int NoOfWorkingDaysInThePeriod)
         {   
             Double calculateTarget=null;
             calculateTarget = (((GoalTarget/NoOfWorkingDaysInThePeriod)*noOfWorkingDaysOfThePerson)*AllocationPerc);
             
             return calculateTarget;
         }
		 
		//Method to calculate Target DAILY
		 public Double CalculateTarget(Double GoalTarget,Double AllocationPerc,int noOfWorkingDaysOfThePerson)
         {   
             Double calculateTarget=null;
             calculateTarget = (((GoalTarget/5)*noOfWorkingDaysOfThePerson)*AllocationPerc);
             
             return calculateTarget;
         }
		 		 

		//Method to calculate Goal Score
         public Double GoalScoreCalculation(Double taskScore,Double GoalWeightage)
         {  
            Double goalScoreCalculation=null;
            goalScoreCalculation=taskScore*GoalWeightage;
            return goalScoreCalculation;
         }

       //Method to calculate SubSection Score
         public Double SubSectionScoreCalculation(Double goalScoreCalculation,Double SubSectionWeightage)
         {
        	Double subSectionScoreCalculation = null;
            subSectionScoreCalculation=goalScoreCalculation*SubSectionWeightage;
            return subSectionScoreCalculation;
         }
     
         //Method to calculate Section Score
         public Double SectionScoreCalculation(Double subSectionScoreCalculation,Double sectionWeightage)
         {
        	 Double sectionScoreCalculation = null;        	 
        	 sectionScoreCalculation=subSectionScoreCalculation*sectionWeightage;
        	 return sectionScoreCalculation;
         }

         //Method to calculate Team Score
         public Double TeamScoreCalculation(Double  sectionScoreCalculation,Double AllocationPerc)             
         {
        	 Double teamScoreCalculation=null;
        	 teamScoreCalculation=sectionScoreCalculation*AllocationPerc;
        	 return teamScoreCalculation;
         } 
        
          
	 //Database connection close
	@Override
	protected void finalize() throws Throwable
	{
		this.con.close();
	}
}


	

