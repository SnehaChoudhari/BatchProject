package com.empowerment.GoalScoreCalc;

public class TDpRulesDTO {
	
	private int Rule_ID;
	private String Rule_Query;
	private String Derived_Data_Point;
	private String Applicable_Freq;
	private String Derived_Datapoint_Description;
	private String Derived_Datapoint_Algorithm;
	private String Applicability;
	public int getRule_ID() {
		return Rule_ID;
	}
	public void setRule_ID(int rule_ID) {
		Rule_ID = rule_ID;
	}
	public String getRule_Query() {
		return Rule_Query;
	}
	public void setRule_Query(String rule_Query) {
		Rule_Query = rule_Query;
	}
	public String getDerived_Data_Point() {
		return Derived_Data_Point;
	}
	public void setDerived_Data_Point(String derived_Data_Point) {
		Derived_Data_Point = derived_Data_Point;
	}
	public String getApplicable_Freq() {
		return Applicable_Freq;
	}
	public void setApplicable_Freq(String applicable_Freq) {
		Applicable_Freq = applicable_Freq;
	}
	public String getDerived_Datapoint_Description() {
		return Derived_Datapoint_Description;
	}
	public void setDerived_Datapoint_Description(String derived_Datapoint_Description) {
		Derived_Datapoint_Description = derived_Datapoint_Description;
	}
	public String getDerived_Datapoint_Algorithm() {
		return Derived_Datapoint_Algorithm;
	}
	public void setDerived_Datapoint_Algorithm(String derived_Datapoint_Algorithm) {
		Derived_Datapoint_Algorithm = derived_Datapoint_Algorithm;
	}
	public String getApplicability() {
		return Applicability;
	}
	public void setApplicability(String applicability) {
		Applicability = applicability;
	}

}
