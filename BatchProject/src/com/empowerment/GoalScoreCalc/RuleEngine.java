package com.empowerment.GoalScoreCalc;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.empowerment.GoalScoreCalc.RuleEngineDao;
import com.empowerment.GoalScoreCalc.RuleEngineDTO;
public class RuleEngine {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		System.out.println("Rule Engine Started.......");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date processDate = null;
		java.util.Date startDate = null;
		java.util.Date endDate = null;
		System.out.println("Start Date - " + args[0]);
		System.out.println("End Date - " + args[1]);
		System.out.println("Frequency - " + args[2]); 
		
        
        try {
        	/*processDate = dateFormat.parse("1/1/2017");
            System.out.println("Date parsed = " + dateFormat.format(processDate));
		    java.sql.Date processsedDate = new java.sql.Date(processDate.getTime()); 
		    startDate = dateFormat.parse("10/1/2017");
            System.out.println("Start Date parsed = " + dateFormat.format(startDate));
		    java.sql.Date startDate_sql = new java.sql.Date(startDate.getTime());
		    endDate = dateFormat.parse("10/1/2017");
            System.out.println("End Date parsed = " + dateFormat.format(endDate));
		    java.sql.Date endDate_sql = new java.sql.Date(endDate.getTime());
		    List ruleEngineDTO;
		    //Daily Frequency
		    String Frequency = new String("Daily");
		    //Monthly Frequency
		    String FrequencyMonthly = new String("Monthly");
		    //Quarterly Frequency
		    String FrequencyQuarterly = new String("Quarterly");*/
        	
        	
        	String dateStart=  args[0];
        	startDate = dateFormat.parse(dateStart);
            System.out.println("Start Date parsed = " + startDate);
 		    java.sql.Date startDate_sql = new java.sql.Date(startDate.getTime());
        	
        	String dateEnd=  args[1];
        	endDate = dateFormat.parse(dateEnd);
            System.out.println("End Date parsed = " + endDate);
 		    java.sql.Date endDate_sql = new java.sql.Date(endDate.getTime());
        	
        	String Frequency =args[2];
			RuleEngineDao ruleEngineDao = new RuleEngineDao();
			//ruleEngineDao.getRules(processsedDate, Frequency);
			//ruleEngineDao.getRules(processsedDate, FrequencyMonthly);
			//ruleEngineDao.getRules(processsedDate, FrequencyQuarterly);
			//ruleEngineDao.getRules(startDate_sql, endDate_sql, Frequency);
			//ruleEngineDao.getRules(startDate_sql, endDate_sql, Frequency);
			
			
			//ruleEngineDao.getRules(351295,10,1,31017,startDate_sql,endDate_sql);
			ruleEngineDao.getRules(startDate_sql, endDate_sql, Frequency);
			
		}
		catch(SQLException sql)
		{
			sql.printStackTrace();
		}
		catch (ParseException e) {
            e.printStackTrace();
        }
   }
}
